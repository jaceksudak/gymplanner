export default {
    "activities": "Activities",
    "add": "ADD ACTIVITY",
    "edit": "EDIT ACTIVITY",
    "namePlaceholder": "Type activity name",
    "instructorNamePlaceholder": "Type instructor name",
    "gymplanner": {
        "general": {
            "error": {
                "invalid_request": "Activity naming error",
                "validation": "Error",
                "unhandled": "Unhandled error",
            }
        },
    },
}