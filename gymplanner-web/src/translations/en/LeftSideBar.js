export default {
    "timetables": "Plans",
    "add": "ADD PLAN",
    "addPlaceholder": "Type plan name",
    "gymplanner": {
        "general": {
            "error": {
                "invalid_request": "Plan naming error",
                "validation": "Error",
            }
        },
    },
}