export default {
    "login": "LOGIN",
    "loginPlaceholder": "Type your login...",
    "passwordPlaceholder": "Type your password...",
    "register": "REGISTER",
    "logout": "LOGOUT",
    "gymplanner": {
        "general": {
            "error": {
                "invalid_request": "Registration failed",
                "validation": "Both fields are required",
            }
        },
        "account": {
            "error": {
                "bad_credentials": "Incorrect login or password",
            }
        }
    },
}