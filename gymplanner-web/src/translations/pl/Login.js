export default {
    "login": "ZALOGUJ",
    "loginPlaceholder": "Wpisz swój login...",
    "passwordPlaceholder": "Wpisz swoje hasło...",
    "register": "ZAREJESTRUJ",
    "logout": "WYLOGUJ",
    "gymplanner": {
        "general": {
            "error": {
                "invalid_request": "Błąd rejestracji",
                "validation": "Oba pola sa wymagane",
            }
        },
        "account": {
            "error": {
                "bad_credentials": "Niepoprawny login lub hasło",
            }
        }
    },
}