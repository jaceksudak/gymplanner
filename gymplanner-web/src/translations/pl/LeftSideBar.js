export default {
    "add": "DODAJ PLAN",
    "timetables": "Plany zajęć",
    "addPlaceholder": "Podaj nazwę planu",
    "gymplanner": {
        "general": {
            "error": {
                "invalid_request": "Błąd nazwy planu",
                "validation": "Błąd",
            }
        },
    },
}