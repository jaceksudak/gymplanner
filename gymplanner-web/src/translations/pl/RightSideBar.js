export default {
    "activities": "Aktywności",
    "add": "DODAJ AKTYWNOŚĆ",
    "edit": "EDYTUJ AKTYWNOŚĆ",
    "namePlaceholder": "Wpisz nazwę aktywności",
    "instructorNamePlaceholder": "Wpisz imię instruktora",
    "gymplanner": {
        "general": {
            "error": {
                "invalid_request": "Błąd nazwy aktywności",
                "validation": "Błąd",
                "unhandled": "Unhandled error",
            }
        },
    },
}