import React from 'react';
import { connect } from 'react-redux';
import TopBar from '../../pages/TopBar';
import LeftSideBar from '../../pages/LeftSideBar';
import RightSideBar from '../../pages/RightSideBar';
import styled from 'styled-components/macro';
import { node, bool } from 'prop-types';
import store from '../../modules/store'

const AuthorizedLayout = ({ children }) => {
    return (
        <StyledContainer>
            <TopBar />
            <LeftSideBar />
            {children}
            <RightSideBar />
        </StyledContainer>
    )
};

const NonAuthorizedLayout = ({ children }) => {
    return <div>{children}</div>;
};

const decorate = connect(
    store.select(models => ({
        isLoggedIn: models.user.isLoggedIn,
    })),
);

const AppLayout = ({ children, isLoggedIn }) => {
    return (
        <>
            {isLoggedIn ? (
                <AuthorizedLayout>{children}</AuthorizedLayout>
            ) : (
                <NonAuthorizedLayout>{children}</NonAuthorizedLayout>
            )}
        </>
    );
};

export default decorate(AppLayout);

AuthorizedLayout.propTypes = {
    children: node.isRequired,
};

NonAuthorizedLayout.propTypes = {
    children: node.isRequired,
};

AppLayout.propTypes = {
    children: node.isRequired,
    isLoggedIn: bool.isRequired,
};

const StyledContainer = styled.div`
    position: relative;
    margin-left: 250px;
    margin-right: 250px;
`;
