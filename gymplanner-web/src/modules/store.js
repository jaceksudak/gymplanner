import { init } from '@rematch/core';
import selectPlugin from '@rematch/select';
import { createPersistMiddleware, loadState } from '../utils/persistStore';
import user from './user';
import timetable from './timetable';
import activity from './activity';
import plan from './plan';


const store = init( {
    models: { user, timetable, activity, plan },
    redux: {
        initialState: loadState(),
        middlewares: [createPersistMiddleware(['user.token', 'user.language'])],
        rootReducers: { RESET_APP: () => undefined }
    },
    plugins: [selectPlugin()],
});

export default store;