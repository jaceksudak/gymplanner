import {getRequest, putRequest, patchRequest, deleteRequest} from '../../services/ApiInterceptor';

const initialState = {
    list: null,
    selectedAoId: null,
};

const planModel = {
    state: initialState,
    reducers: {
        aosFetched: (state, list) => ({ ...state, list }),
        aoSelected: (state, selectedAoId) => ({ ...state, selectedAoId})
    },
    effects: () => ({
        selectAo(aoId) {
            this.aoSelected(aoId);
        },
        async deleteAo(aoId) {
            try {
                const deleted = await deleteRequest('activityOccurrence', {activityOccurrenceId: aoId}, {});
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async addAo(payload) {
            try {
                const putted = await putRequest('activityOccurrence', payload, {});
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async fetchAll(timeTableId) {
            try {
                if (!!timeTableId) {
                    const aos = await getRequest(`activityOccurrence?timeTableId=${timeTableId}`);
                    this.aosFetched(aos.activityOccurrencesPerDayLine);
                }
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
    }),
    selectors: slice => ({
        activityOccurrences: () => slice(ao => ao.list),
        isSelected: () => slice(ao => !!ao.selected),
        selected: () => slice(ao => ao.selectedAoId),
    }),
};

export default planModel;