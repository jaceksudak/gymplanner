import { postRequest } from '../../services/ApiInterceptor';

const initialState = {
    token: null,
    language: 'pl'
};

const userModel = {
    state: initialState,
    reducers: {
        sessionStart: (state, { token }) => ({ ...state, token }),
        sessionFinish: state => ( { ...state, token: null }),
        languageChanged: (state, language) => ({
            ...state,
            language,
        }),
    },
    effects: (dispatch) => ({
        async login(payload) {
            try {
                const { accessToken: token } = await postRequest('login', payload, {});
                this.sessionStart({ token });
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        logout() {
            try {
                this.sessionFinish();
                dispatch({ type: 'RESET_APP' });
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async refreshToken() {
            try {
                const {accessToken: token} = await postRequest('refresh');
                this.sessionStart({token});
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async register(payload) {
            try {
                await postRequest('register', payload);
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
    }),
    selectors: slice => ({
        isLoggedIn: () => slice(user => !!user.token),
        currentLanguage: () => slice(user => user.language),
    }),
};

export default userModel;