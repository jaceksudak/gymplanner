import { getRequest, putRequest } from '../../services/ApiInterceptor';

const initialState = {
    list: [],
    selected: null,
};

const timeTableModel = {
    state: initialState,
    reducers: {
        timetablesFetched: (state, list) => ({ ...state, list }),
        timetableSelected: (state, selected) => ({ ...state, selected})
    },
    effects: () => ({
        selectTimeTable(timeTableId) {
            this.timetableSelected(timeTableId);
        },
        async fetchTimetable() {
            try {
                const timetables = await getRequest('timetable');
                this.timetablesFetched(timetables);
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async addTimetable(timeTableName) {
            try {
                const putted = await putRequest('timetable', timeTableName, {});
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
    }),
    selectors: slice => ({
        timetables: () => slice(timetable => timetable.list),
        isSelected: () => slice(timetable => !!timetable.selected),
        selected: () => slice(timetable => timetable.selected),
    }),
};

export default timeTableModel;