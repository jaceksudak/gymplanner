import { getRequest, putRequest, patchRequest } from '../../services/ApiInterceptor';

const initialState = {
    list: [],
    selected: null,
};

const activityModel = {
    state: initialState,
    reducers: {
        activitiesFetched: (state, list) => ({ ...state, list }),
        activitySelected: (state, selected) => ({ ...state, selected})
    },
    effects: () => ({
        selectActivity(activityId) {
            this.activitySelected(activityId);
        },
        async fetchActivities() {
            try {
                const activities = await getRequest('activity');
                this.activitiesFetched(activities);
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async addActivity(payload) {
            try {
                const putted = await putRequest('activity', payload);
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
        async editActivity(payload) {
            try {
                const patched = await patchRequest('activity', payload);
            } catch (e) {
                const error = (e && e.key) || 'gymplanner.general.error.unhandled';
                throw Error(error);
            }
        },
    }),
    selectors: slice => ({
        activities: () => slice(activity => activity.list),
        isSelected: () => slice(activity => !!activity.selected),
        selected: () => slice(activity => activity.selected),
    }),
};

export default activityModel;