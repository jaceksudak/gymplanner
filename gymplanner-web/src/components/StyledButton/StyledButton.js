import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Button } from "@material-ui/core/index";


const StyledButton = styled(Button)`
    display: block;
    background-color: #3399ff;
    padding: 5px 20px;
    margin: auto;
    margin-top: 5px;
    margin-bottom: 5px;
    text-transform: none;
`;

export default StyledButton;