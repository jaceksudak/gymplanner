import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { bool } from 'prop-types';
import store from '../../modules/store';



const decorate = compose(
    connect(
        store.select(models => ({
            isLoggedIn: models.user.isLoggedIn,
        })),
    ),
);

const AuthRoute = ({ isLoggedIn, ...props }) => {
    if (!isLoggedIn) {
        return <Redirect to="/login" />;
    }
    return <Route {...props} />;
};

AuthRoute.propTypes = {
    isLoggedIn: bool.isRequired,
};

export default decorate(AuthRoute);