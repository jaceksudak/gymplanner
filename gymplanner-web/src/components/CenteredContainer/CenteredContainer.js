import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import styled from 'styled-components/macro';


const CenteredContainer = styled.div`
    margin-left: auto;
    margin-right: auto;
`;

export default CenteredContainer;