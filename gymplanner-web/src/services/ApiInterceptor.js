import mitt from 'mitt';
import storageService from './StorageService';

const API_CONTEXT_ROOT = 'api';
const API_URL = 'http://localhost:8080/gymplanner-0.0.1-SNAPSHOT/' + API_CONTEXT_ROOT;

export const events = mitt();

export const API_EVENTS = {
    tokenExpired: 'TokenExpired',
    refreshToken: 'RefreshToken'
};

const successCallback = result => {
    if (result.headers.get('Token-Status') === 'refresh') {
        events.emit(API_EVENTS.refreshToken)
    }

    return result.text().then(data => {
        return data ? JSON.parse(data) : {};
    });
};

const errorCallback = (error = {}) => {
    if (error.status === 401 && error.url !== `${API_URL}/login` && error.url !== `${API_URL}/logout`) {
        return events.emit(API_EVENTS.tokenExpired, { error });
    }

    return error.text().then(data => {
        return data ? JSON.parse(data) : {};
    });
};

const fetchInterceptor = async (endpoint, config) => {
    const url = `${API_URL}/${endpoint}`;
    const result = await fetch(url, config);
    let data;

    if (result.ok) {
        data = await successCallback(result);
        return Promise.resolve(data);
    }

    data = await errorCallback(result);
    return Promise.reject(data);
};

const mergeConfig = (config = {}) => {
    const { token } = storageService.get('user', {});

    return {
        ...config,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
            ...config.headers,
        }
    };
};

export const getRequest = async (endpoint, config = {}) =>
    fetchInterceptor(
        endpoint,
        mergeConfig({ ...config, method: 'GET' })
    );


export const postRequest = async (endpoint, payload, config = {}) =>
    fetchInterceptor(
        endpoint,
        mergeConfig({ ...config, method: 'POST', body: JSON.stringify(payload) }),
    );

export const putRequest = async (endpoint, payload, config = {}) =>
    fetchInterceptor(
        endpoint,
        mergeConfig({ ...config, method: 'PUT', body: JSON.stringify(payload) })
    );

export const patchRequest = async (endpoint, payload, config = {}) =>
    fetchInterceptor(
        endpoint,
        mergeConfig({ ...config, method: 'PATCH', body: JSON.stringify(payload) })
    );

export const deleteRequest = async (endpoint, payload, config = {}) =>
    fetchInterceptor(
        endpoint,
        mergeConfig({ ...config, method: 'DELETE', body: JSON.stringify(payload) })
    );
