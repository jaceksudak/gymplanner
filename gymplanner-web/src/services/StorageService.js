import lockr from 'lockr';

const storageInterface = lockr;

storageInterface.prefix = 'gymplanner_';

export default {
    get: (key, defaultValue = null) => storageInterface.get(key, defaultValue),
    getAll: (includeKeys = false) => storageInterface.getAll(includeKeys),
    set: (key, value) => {
        storageInterface.set(key, value);
    },
    remove: (key) => storageInterface.rm(key),
    flush: () => storageInterface.flush(),
};