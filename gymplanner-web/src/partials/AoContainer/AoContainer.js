import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import styled from 'styled-components/macro';

const AoContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 60px;
`;

export default AoContainer;