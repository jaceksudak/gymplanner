import React from 'react';
import AoContainer from "../AoContainer/AoContainer";
import {useTranslation} from "react-i18next";
import CenteredContainer from "../../components/CenteredContainer/CenteredContainer";

const DayLineHeader = () => {
    const { t } = useTranslation('Main');

    return (
        <div style={{ display: 'inline-block', width: '60px' , margin: '5px', marginTop: '10px'}}>
            <AoContainer style={{height: '30px'}}><CenteredContainer>{t('hourHeader')}</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>8-9</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>9-10</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>10-11</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>11-12</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>12-13</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>13-14</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>14-15</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>15-16</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>16-17</CenteredContainer></AoContainer>
            <AoContainer><CenteredContainer>17-18</CenteredContainer></AoContainer>
        </div>
    );
};

export default DayLineHeader;