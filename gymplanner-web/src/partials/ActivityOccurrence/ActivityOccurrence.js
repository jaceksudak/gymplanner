import React from 'react';
import AoContainer from "../AoContainer/AoContainer";
import CenteredContainer from "../../components/CenteredContainer/CenteredContainer";
import styled from 'styled-components/macro';
import {Button} from "@material-ui/core/index";

const ActivityOccurrence = ( {ao, selectedActivityId, handleAdd, handleDelete }) => {

const StyledButton = styled(Button)`
    display: block;
    background-color: #3399ff;
    margin: auto;
    text-transform: none;
`;

    if (!ao.id) {
        return (
            <AoContainer key = {100*ao.dayLineId + ao.index}>
                <CenteredContainer>
                    <StyledButton
                        style={{width: '100%', height: '100%'}}
                        disabled={!selectedActivityId}
                        onClick={() => {
                            handleAdd(ao.index, selectedActivityId, ao.dayLineId)
                        }}
                    >
                        <h2>+</h2>
                    </StyledButton>
                </CenteredContainer>
            </AoContainer>);
    } else {
        return (
            <AoContainer key = {100*ao.dayLineId + ao.index}>
                <CenteredContainer>
                    <StyledButton
                        style={{width: '100%', height: '100%', backgroundColor: '#99ccff'}}
                        onClick={() => {
                            handleDelete(ao.id)
                        }}
                    >
                        <span>{ao.activityName}</span>
                        <br/>
                        <span>{ao.activityInstructorName}</span>
                    </StyledButton>
                </CenteredContainer>
            </AoContainer>);
    }
};

export default ActivityOccurrence;