import pick from 'lodash/fp/pick';
import throttle from 'lodash/fp/throttle';
import StorageService from '../services/StorageService';


export function loadState() {
    return StorageService.getAll(true).reduce((acc, val) => {
        acc = { ...acc, ...val };
        return acc;
    }, {});
}

export const createPersistMiddleware = (persistPaths, throttleLimit = 100) => {
    const save = throttle(throttleLimit, persistState);
    return store => next => action => {
        const result = next(action);
        const slicedState = pick(persistPaths, store.getState());
        save(slicedState);
        return result;
    };
};

function persistState(state) {
    Object.keys(state).forEach(key => {
        StorageService.set(key, state[key]);
    });
}

