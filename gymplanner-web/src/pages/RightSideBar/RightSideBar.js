import React, { useEffect } from 'react';
import { Route } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import store from '../../modules/store';
import { Formik, Field, Form } from "formik";
import styled from 'styled-components/macro';
import {TextField} from "@material-ui/core/index";
import StyledButton from '../../components/StyledButton';


const decorate = connect(
    store.select(models => ({
        activities: models.activity.activities,
        isSelected: models.activity.isSelected,
        selected: models.activity.selected,
    })),
    dispatch => ({
            fetchActivities: dispatch.activity.fetchActivities,
            addActivity: dispatch.activity.addActivity,
            editActivity: dispatch.activity.editActivity,
            selectActivity: dispatch.activity.selectActivity,
            fetchAos: dispatch.plan.fetchAll,
        }
    )
);

const RightSideBar = ({
                          activities,
                          fetchActivities,
                          fetchAos,
                          addActivity,
                          editActivity,
                          selectActivity,
                          isSelected,
                          selected
                      }) => {

    const {t} = useTranslation('RightSideBar');

    const handleOnClick = (id) => {
        selectActivity(id);
    };

    const activitiesButtons = activities.map(activity => {
        if (isSelected && activity.id === selected) {
            return <SelectedButton key={activity.id} onClick={() => handleOnClick(null)}>
                <span>{activity.name}</span>
                <br/>
                <span>{activity.instructorName}</span>
            </SelectedButton>
        } else {
            return <StyledButton key={activity.id} onClick={() => handleOnClick(activity.id)}>
                <span>{activity.name}</span>
                <br/>
                <span>{activity.instructorName}</span>
            </StyledButton>
        }
    });

    useEffect(() => {
        fetchActivities();
    }, []);


    const handleAddActivity = async (payload, { setSubmitting, setFieldError }) => {
        try {
            if (isSelected) {
                await editActivity({ id: selected, ...payload });
                await fetchActivities();
                await fetchAos();
            } else {
                await addActivity(payload);
                await fetchActivities();
            }
        } catch (e) {
            setFieldError('general', e.message);
        } finally {
            setSubmitting(false);
        }
    };

    const addActivityForm = (
        <Formik
            initialValues={{
                activityName: "",
                instructorName: "",
            }}
            onSubmit={handleAddActivity}
        >
            { ({ errors, isSubmitting, handleSubmit }) => (
                <Form>
                    <Field
                        placeholder={t('namePlaceholder')}
                        name="activityName"
                        type="input"
                        as={TextField}
                    />
                    <Field
                        placeholder={t('instructorNamePlaceholder')}
                        name="instructorName"
                        type="input"
                        as={TextField}
                    />
                    <div style={{
                        backgroundColor: '#3399ff',
                        marginTop: '10px',
                        marginBottom: '10px',
                    }}>
                        <StyledButton
                            disabled={isSubmitting}
                            fullWidth='true'
                            type='button'
                            onClick={() => {
                                handleSubmit();
                            }}
                        >
                            {selected ? t('edit') : t('add')}
                        </StyledButton>
                    </div>
                    <div style={{width: '100%'}}>
                        <span>{t(errors.general || '')}</span>
                    </div>
                </Form>
            ) }
        </Formik>
    );

    return (
        <RightSideBarWrapper>
            <StyledActivitiesListContainer>
                <h2 style={{
                    marginTop: '10px',
                    marginLeft: '10px'
                }}>{t('activities')}:</h2>
                {activitiesButtons}
            </StyledActivitiesListContainer>
            <ManageActivitiesContainer>
                <CenteredManageActivitiesContainer>
                    {addActivityForm}
                </CenteredManageActivitiesContainer>
            </ManageActivitiesContainer>
        </RightSideBarWrapper>
    );
};

export default decorate(RightSideBar);

const RightSideBarWrapper = styled.div`
    position: fixed;
    right: 0;
    top: 0;
    width: 250px;
    height: 100%;
    overflow: auto;
    border-left: solid 1px;
    background-color: #99ccff
`;

const SelectedButton = styled(StyledButton)`
    background-color: #339371;
    color: white;
`;

const StyledActivitiesListContainer = styled.div`
    width: 100%;
    height: 75%;
    overflow: auto;
`;

const ManageActivitiesContainer = styled.div`
    position: absolute;
    border-top: solid 1px;
    bottom: 0px;
    height: 25%;
    width: 100%;
    overflow: auto;
    background-color: #99ccff
`;

const CenteredManageActivitiesContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
    overflow: auto;
`;