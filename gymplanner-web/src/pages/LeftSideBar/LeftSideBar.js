import React, { useEffect } from 'react';
import { Route } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import store from '../../modules/store';
import { Formik, Field, Form } from "formik";
import styled from 'styled-components/macro';
import {TextField} from "@material-ui/core/index";
import StyledButton from '../../components/StyledButton';


const decorate = connect(
    store.select(models => ({
        timetables: models.timetable.timetables,
        isSelected: models.timetable.isSelected,
        selected: models.timetable.selected,
    })),
    dispatch => ({
            fetchTimetable: dispatch.timetable.fetchTimetable,
            addTimetable: dispatch.timetable.addTimetable,
            selectTimeTable: dispatch.timetable.selectTimeTable,
        }
    )
);


const LeftSideBar = ({
                         timetables,
                         fetchTimetable,
                         addTimetable,
                         selectTimeTable,
                         isSelected,
                         selected
}) => {
    const { t } = useTranslation('LeftSideBar');

    const handleOnClick = (id) => {
        selectTimeTable(id);
    };

    const timetablesButtons = timetables.map(tt => {
        if (isSelected && tt.id === selected) {
            return <SelectedButton key={tt.id} onClick={() => handleOnClick(null)}>{tt.name}</SelectedButton>
        } else {
            return <StyledButton key={tt.id} onClick={() => handleOnClick(tt.id)}>{tt.name}</StyledButton>
        }
    });

    useEffect(() => {
        fetchTimetable();
    },[]);


    const handleAddTimeTable = async (payload, { setSubmitting, setFieldError }) => {
        try {
            await addTimetable(payload);
            await fetchTimetable();
        } catch (e) {
            setFieldError('general', e.message);
        } finally {
            setSubmitting(false);
        }
    };

    const addTimeTableForm = (
        <Formik
            initialValues={{
                timeTableName: "",
            }}
            onSubmit={handleAddTimeTable}
        >
            { ({ values, errors, isSubmitting, handleSubmit }) => (
                <Form>
                    <Field
                        placeholder={t('addPlaceholder')}
                        name="timeTableName"
                        type="input"
                        as={TextField}
                    />
                    <div style={{
                        backgroundColor: '#3399ff',
                        marginTop: '10px',
                        marginBottom: '10px',
                    }}>
                        <StyledButton
                            disabled={isSubmitting}
                            fullWidth='true'
                            type='button'
                            onClick={() => {
                                handleSubmit();
                            }}
                        >
                            {t('add')}
                        </StyledButton>
                    </div>
                    <div style={{width: '100%'}}>
                        <span>{t(errors.general || '')}</span>
                    </div>
                </Form>
            ) }
        </Formik>
    );

    return (
        <LeftSideBarWrapper>
            <StyledTimeTablesListContainer>
                <h2 style={{
                    marginTop: '10px',
                    marginLeft: '10px'
                }}>{t('timetables')}:</h2>
                {timetablesButtons}
            </StyledTimeTablesListContainer>
            <ManageTimeTablesContainer>
                <CenteredManageTimeTablesContainer>
                    {addTimeTableForm}
                </CenteredManageTimeTablesContainer>
            </ManageTimeTablesContainer>
        </LeftSideBarWrapper>
    );
};

export default decorate(LeftSideBar);

const LeftSideBarWrapper = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    width: 250px;
    height: 100%;
    overflow: auto;
    border-right: solid 1px;
    background-color: #99ccff
`;

const SelectedButton = styled(StyledButton)`
    background-color: #339371;
    color: white;
`;

const StyledTimeTablesListContainer = styled.div`
    width: 100%;
    height: 75%;
    overflow: auto;
`;

const ManageTimeTablesContainer = styled.div`
    position: absolute;
    border-top: solid 1px;
    bottom: 0px;
    height: 25%;
    width: 100%;
    overflow: auto;
    background-color: #99ccff
`;

const CenteredManageTimeTablesContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
    overflow: auto;
`;