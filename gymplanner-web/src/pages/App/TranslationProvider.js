import * as React from 'react';
import { I18nextProvider } from 'react-i18next';
import i18n from 'i18next';
import { connect } from 'react-redux';
import { string } from 'prop-types';

import store from '../../modules/store';

const STRING_BUNDLES = [
    'Login',
    'LeftSideBar',
    'RightSideBar',
    'Main'
];

const getBundlesForLocale = locale =>
    STRING_BUNDLES.reduce((bundles, bundleName) => {
        return {
            ...bundles,
            [bundleName]: require(`../../translations/${locale}/${bundleName}.js`).default,
        };
    }, {});

i18n.init({
    fallbackLng: 'pl',
    debug: process.env.NODE_ENV === 'development',

    interpolations: {
        escapeValue: false,
    },

    resources: {
        pl: getBundlesForLocale('pl'),
        en: getBundlesForLocale('en'),
    },

    react: {
        wait: true,
    }
});

export const SUPPORTED_LANGUAGES = [
    {
        code: 'pl',
        name: 'polski'
    },
    {
        code: 'en',
        name: 'english',
    },
];

const decorate = connect(
    store.select(models => ({
        currentLanguage: models.user.currentLanguage,
    })),
);

const TranslationProvider = ({ currentLanguage, ...props }) => {
    i18n.changeLanguage(currentLanguage);
    return (
        <span lang={currentLanguage}>
            <I18nextProvider i18n={i18n} {...props} />
        </span>
    );
};

TranslationProvider.propTypes = {
    currentLanguage: string.isRequired,
};

export default decorate(TranslationProvider);