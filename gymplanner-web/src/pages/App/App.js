import React, { useEffect, useCallback } from 'react';
import { Provider as StoreProvider} from 'react-redux';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { node } from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import store from '../../modules/store';
import TranslationProvider from './TranslationProvider';
import Main from '../Main';
import Login from '../Login';
import AppLayout from '../../layouts/AppLayout';
import AuthRoute from '../../components/AuthRoute';
import {getRequest, postRequest} from '../../services/ApiInterceptor';

export default () => {



    return (
        <>
            <CssBaseline />
            <Wrappers>
                <Switch>
                    <AuthRoute exact path="/main" component={Main} />
                    <Route path="/login" component={Login} />
                    <Redirect to="/main" />
                </Switch>
            </Wrappers>
        </>
    );
};

const Wrappers = ({ children }) => (
    <StoreProvider store={store}>
        <TranslationProvider>
            <BrowserRouter basename={process.env.PUBLIC_URL}>
                <AppLayout> {children} </AppLayout>
            </BrowserRouter>
        </TranslationProvider>
    </StoreProvider>
);

Wrappers.propTypes = {
    children: node.isRequired,
};
