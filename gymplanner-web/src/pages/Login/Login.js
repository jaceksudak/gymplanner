import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import store from '../../modules/store';
import { Formik, Field, Form } from "formik";
import styled from 'styled-components/macro';
import StyledButton from '../../components/StyledButton';
import { TextField } from "@material-ui/core";


const decorate = connect(
    store.select(models => ({
        isLoggedIn: models.user.isLoggedIn,
        currentLanguage: models.user.currentLanguage
    })),
    dispatch => ({
            login: dispatch.user.login,
            register: dispatch.user.register,
            changeLanguage: dispatch.user.languageChanged
        }
    )
);


const Login = ({
                    isLoggedIn,
                    login,
                    register,
                    history
                }) => {

    const { t } = useTranslation('Login');

    useEffect(() => {
        if (isLoggedIn) {
            history.push('dashboard');
        }
    });

    const handleSubmit = async (payload, { setSubmitting, setFieldError }) => {
        try {
            if (!!payload.register) {
                await register(payload);
            }
            await login(payload);
        } catch (e) {
            setFieldError('general', e.message);
        } finally {
            setSubmitting(false);
        }
    };

    return (
        <StyledLoginWrapper>
            <Formik
                initialValues={{
                    login: "",
                    password: "",
                    register: false,
                }}
                onSubmit={handleSubmit}
            >
                { ({ errors, isSubmitting, setFieldValue, handleSubmit }) => (
                    <Form>
                        <Field
                            placeholder={t('loginPlaceholder')}
                            name="login"
                            type="input"
                            as={TextField}
                        />
                        <div>
                            <Field
                                placeholder={t('passwordPlaceholder')}
                                name="password"
                                type="password"
                                as={TextField}
                            />
                        </div>
                            <StyledButton
                                style={{width: "130px"}}
                                disabled={isSubmitting}
                                type="button"
                                onClick={() => {
                                    setFieldValue('register', false, true);
                                    handleSubmit();
                                } }
                            >
                                {t('login')}
                            </StyledButton>
                            <StyledButton
                                style={{width: "130px"}}
                                disabled={isSubmitting}
                                type="button"
                                onClick={() => {
                                    setFieldValue('register', true, true);
                                    handleSubmit();
                                } }
                            >
                                {t('register')}
                            </StyledButton>
                        <span>{t(errors.general)}</span>
                    </Form>
                ) }
            </Formik>
        </StyledLoginWrapper>
    );
};

export default decorate(Login);

const StyledLoginWrapper = styled.div`
    position: fixed;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #99ccff;
    height: 100%;
    width: 100%;
`;
