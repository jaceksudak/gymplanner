import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import {API_EVENTS, events} from '../../services/ApiInterceptor';
import store from '../../modules/store';
import StyledButton from '../../components/StyledButton';
import styled from 'styled-components/macro';


const decorate = connect(
    store.select(models => ({
        isLoggedIn: models.user.isLoggedIn,
        currentLanguage: models.user.currentLanguage
    })),
    dispatch => ({
            logout: dispatch.user.logout,
            refreshToken: dispatch.user.refreshToken,
            changeLanguage: dispatch.user.languageChanged,
        }
    )
);


const TopBar = ({
    isLoggedIn,
    logout,
    refreshToken,
    changeLanguage,
    currentLanguage
}) => {

    const { t } = useTranslation('Login');

    useEffect(() => {
        events.on(API_EVENTS.tokenExpired, () => {
            logout();
        });
        events.on(API_EVENTS.refreshToken, () => refreshToken());
        return () => {
            events.off(API_EVENTS.tokenExpired);
            return events.off(API_EVENTS.refreshToken);
        }
    }, [logout, refreshToken]);

    return (
        <StyledTopBarWrapper>
            <StyledButtonWithMargin
                disabled={!isLoggedIn}
                type="submit"
                onClick={logout}
            >
                {t('logout')}
            </StyledButtonWithMargin>
            <StyledButtonWithMargin
                disabled={'pl' === currentLanguage}
                type="submit"
                onClick={() => changeLanguage('pl')}
            >
                PL
            </StyledButtonWithMargin>
            <StyledButtonWithMargin
                disabled={'en' === currentLanguage}
                type="submit"
                onClick={() => changeLanguage('en')}
            >
                EN
            </StyledButtonWithMargin>
        </StyledTopBarWrapper>
    );
};

export default decorate(TopBar);

const StyledTopBarWrapper = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    border-bottom: solid 1px;
    background-color: #99ccff;
`;

const StyledButtonWithMargin = styled(StyledButton)`
    margin-left: 5px;
    margin-right: 5px;
`;
