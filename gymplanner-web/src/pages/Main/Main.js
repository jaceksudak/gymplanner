import React, { useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import { getRequest } from '../../services/ApiInterceptor';
import { Button } from "@material-ui/core";
import DayLineHeader from "../../partials/DayLineHeader/DayLineHeader";
import store from "../../modules/store";
import AoContainer from "../../partials/AoContainer/AoContainer";
import ActivityOccurrence from "../../partials/ActivityOccurrence/ActivityOccurrence";
import {useTranslation} from "react-i18next";
import CenteredContainer from "../../components/CenteredContainer/CenteredContainer";

const decorate = connect(
    store.select(models => ({
        activityOccurrences: models.plan.activityOccurrences,
        isSelected: models.plan.isSelected,
        selected: models.plan.selected,
        selectedTimeTable: models.timetable.selected,
        timeTableIdSelected: models.timetable.isSelected,
        selectedActivityId: models.activity.selected,
    })),
    dispatch => ({
            fetchAll: dispatch.plan.fetchAll,
            deleteAo: dispatch.plan.deleteAo,
            addAo: dispatch.plan.addAo,
        }
    )
);

const Main = ( {activityOccurrences, fetchAll, selectedTimeTable, selectedActivityId, addAo, deleteAo}) => {

    const { t } = useTranslation('Main');

    useEffect(() => {
        fetchAll(selectedTimeTable);
    },[selectedTimeTable]);


    const dayLine = (header, index) => {
        return (
            <div style={{ display: 'inline-block', width: '180px' , margin: '5px', marginTop: '10px'}}>
                <AoContainer style={{height: '30px'}}><CenteredContainer>{t(header)}</CenteredContainer></AoContainer>
                {aoContainers(index)}
            </div>
        );
    };

    const handleAdd = async (index, activityId, dayLineId) => {
        await addAo({index, activityId, dayLineId});
        fetchAll(selectedTimeTable);
    };

    const handleDelete = async (aoId) => {
        await deleteAo(aoId);
        fetchAll(selectedTimeTable);
    };

    const aoContainers = (index) => {
        return activityOccurrences[index].map(ao => {
            return <ActivityOccurrence key={100*ao.dayLineId + ao.index} ao={ao} selectedActivityId={selectedActivityId} handleAdd={handleAdd} handleDelete={handleDelete}/>
        })
    };

    if (!activityOccurrences || !selectedTimeTable) {
        return <h1 style={{padding: '20px'}}>Wybierz plan zajęć</h1>
    } else {
        return (
            <div style={{
                backgroundColor: '#99ccff'
            }}>
                <DayLineHeader/>
                {dayLine('monHeader', 0)}
                {dayLine('tueHeader', 1)}
                {dayLine('wedHeader', 2)}
                {dayLine('thuHeader', 3)}
                {dayLine('friHeader', 4)}
            </div>

        );
    }
};

export default decorate(Main);
