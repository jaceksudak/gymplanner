package pl.jaceksudak.gymplanner.example;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.jaceksudak.gymplanner.domain.*;
import pl.jaceksudak.gymplanner.domain.service.DataCreator;

@Component
public class ExampleDataCreator {

    private final DataCreator dataCreator;

    public ExampleDataCreator(DataCreator dataCreator) {
        this.dataCreator = dataCreator;
    }

    @Transactional
    public void createExampleTimetable(User user) {

        TimeTable timeTable = dataCreator.createDraftTimetableWithDayLines("Przykład", user);

        Activity activity1 = dataCreator.createActivity(user, "Trener 1", "Zumba");
        Activity activity2 = dataCreator.createActivity(user, "Trener Andrzej", "Fitness");
        Activity activity3 = dataCreator.createActivity(user, "Gosia Iksińska", "Płaski brzuch");
        Activity activity4 = dataCreator.createActivity(user, "Jarosław K", "Power push");

        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(0), activity1, 3);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(0), activity2, 5);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(0), activity3, 6);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(1), activity1, 0);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(1), activity1, 5);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(3), activity3, 3);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(3), activity4, 4);
        dataCreator.createActivityOccurrence(timeTable.getDayLines().get(4), activity1, 2);
    }
}
