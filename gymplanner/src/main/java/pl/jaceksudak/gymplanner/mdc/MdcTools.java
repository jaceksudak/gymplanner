package pl.jaceksudak.gymplanner.mdc;

import org.slf4j.MDC;
import pl.jaceksudak.gymplanner.common.Header;
import pl.jaceksudak.gymplanner.security.jwt.TokenDTO;

import java.util.UUID;

public class MdcTools {

    private static final String ANONYMOUS_USER_ID = "anonymous_gymp_user";
    private static final String ANONYMOUS_SESSION_ID = "anonymous_gymp_session";

    public static void fillMdc(TokenDTO tokenDTO) {
        setUserId(tokenDTO.getUserName());
        setSessionId(tokenDTO.getSessionId());
        setRequestId(UUID.randomUUID().toString());
    }

    public static void fillAsAnonymous() {
        setUserId(ANONYMOUS_USER_ID);
        setSessionId(ANONYMOUS_SESSION_ID);
        setRequestId(UUID.randomUUID().toString());
    }

    public static void cleanUpMdc() {
        MDC.remove(Header.USER_ID);
        MDC.remove(Header.SESSION_ID);
        MDC.remove(Header.REQUEST_ID);
    }

    public static void setUserId(String userId) {
        MDC.put(Header.USER_ID, userId);
    }

    public static void setSessionId(String sessionId) {
        MDC.put(Header.SESSION_ID, sessionId);
    }

    public static String getRequestId() {
        return MDC.get(Header.REQUEST_ID);
    }

    public static void setRequestId(String requestId) {
        MDC.put(Header.REQUEST_ID, requestId);
    }
}
