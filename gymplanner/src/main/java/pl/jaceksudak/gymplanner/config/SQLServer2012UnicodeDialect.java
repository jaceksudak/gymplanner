package pl.jaceksudak.gymplanner.config;

import org.hibernate.dialect.SQLServer2012Dialect;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

public class SQLServer2012UnicodeDialect extends SQLServer2012Dialect {

    private static final int MAX_LENGTH = 8000;

    public SQLServer2012UnicodeDialect() {
        registerColumnType(Types.VARCHAR, "nvarchar(MAX)");
        registerColumnType(Types.VARCHAR, MAX_LENGTH, "nvarchar($1)");
        registerColumnType(Types.CLOB, "nvarchar(MAX)");

        registerHibernateType(Types.VARCHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.NVARCHAR, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.BIGINT, StandardBasicTypes.LONG.getName());
        registerHibernateType(Types.DECIMAL, StandardBasicTypes.BIG_DECIMAL.getName());
        registerHibernateType(Types.NUMERIC, StandardBasicTypes.BIG_DECIMAL.getName());
        registerHibernateType(Types.DATE, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.TIMESTAMP, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.TIME, StandardBasicTypes.STRING.getName());
    }
}
