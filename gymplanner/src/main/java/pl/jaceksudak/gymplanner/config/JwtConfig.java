package pl.jaceksudak.gymplanner.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "gymplanner.security.jwt")
public class JwtConfig {

    private String tokenSigningKey;
    private Integer tokenExpirationTime;
    private Integer tokenRenewTime;

    public String getTokenSigningKey() {
        return tokenSigningKey;
    }

    public void setTokenSigningKey(String tokenSigningKey) {
        this.tokenSigningKey = tokenSigningKey;
    }

    public Integer getTokenExpirationTime() {
        return tokenExpirationTime;
    }

    public void setTokenExpirationTime(Integer tokenExpirationTime) {
        this.tokenExpirationTime = tokenExpirationTime;
    }

    public Integer getTokenRenewTime() {
        return tokenRenewTime;
    }

    public void setTokenRenewTime(Integer tokenRenewTime) {
        this.tokenRenewTime = tokenRenewTime;
    }
}
