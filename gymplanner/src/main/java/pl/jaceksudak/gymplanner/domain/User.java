package pl.jaceksudak.gymplanner.domain;

import pl.jaceksudak.gymplanner.security.common.SecurityConstants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "sqUser", sequenceName = "sqUser", allocationSize = 1)
@Table(name ="`User`",
        uniqueConstraints = {
        @UniqueConstraint(columnNames = "login", name = "UC_User_login")
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqUser")
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @Column(nullable = false, unique = true, length = 50)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = SecurityConstants.SALT_SIZE)
    private String salt;

    @OneToMany(mappedBy = "user")
    private List<TimeTable> timeTables = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Activity> activities = new ArrayList<>();

    public User() {
    }

    public User(String login, String password, String salt) {
        this.login = login;
        this.password = password;
        this.salt = salt;
    }

    public void addTimeTable(TimeTable timeTable) {
        this.timeTables.add(timeTable);
        timeTable.setUser(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public List<TimeTable> getTimeTables() {
        return timeTables;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void addActivity(Activity activity) {
        activities.add(activity);
        activity.setUser(this);
    }
}
