package pl.jaceksudak.gymplanner.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(nativeQuery = true, value = "SELECT u.* FROM [User] u WHERE u.login = ?1")
    Optional<User> findOneByLogin(String login);
}
