package pl.jaceksudak.gymplanner.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TimeTableRepository extends JpaRepository<TimeTable, Long> {

    @Query(nativeQuery = true, value = "SELECT tt.* FROM [TimeTable] tt WHERE tt.userId = ?1")
    List<TimeTable> findAllByUserId(Long userId);

    @Query(nativeQuery = true, value =
            "SELECT CASE WHEN EXISTS (" +
            "    SELECT *" +
            "    FROM [TimeTable] tt" +
            "    WHERE tt.userId = ?2 AND tt.id = ?1" +
            ")" +
            "THEN CAST(1 AS BIT)" +
            "ELSE CAST(0 AS BIT) END")
    boolean timeTableBelongsToUser(Long timeTableId, Long userId);

    @Query(nativeQuery = true, value =
            "SELECT CASE WHEN EXISTS (" +
            "    SELECT *" +
            "    FROM [TimeTable] tt" +
            "    WHERE tt.name = ?1" +
            ")" +
            "THEN CAST(1 AS BIT)" +
            "ELSE CAST(0 AS BIT) END")
    boolean timeTableWithNameExists(String timeTableName);
}
