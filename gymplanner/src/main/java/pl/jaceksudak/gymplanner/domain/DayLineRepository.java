package pl.jaceksudak.gymplanner.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DayLineRepository extends JpaRepository<DayLine, Long> {

    @Query(nativeQuery = true, value = "SELECT dl.* FROM [DayLine] dl WHERE dl.timeTableId = ?1 ORDER BY [index] ASC")
    List<DayLine> findAllByTimeTableId(Long timeTableId);

    @Query(nativeQuery = true, value =
            "SELECT CASE WHEN EXISTS (" +
                    "    SELECT *" +
                    "    FROM [DayLine] dl" +
                    "    JOIN [TimeTable] tt ON dl.timeTableId = tt.id" +
                    "    WHERE tt.userId = ?2 AND dl.id = ?1" +
                    ")" +
                    "THEN CAST(1 AS BIT)" +
                    "ELSE CAST(0 AS BIT) END")
    boolean dayLineBelongsToUser(Long dayLineId, Long userId);
}
