package pl.jaceksudak.gymplanner.domain;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "sqTimeTable", sequenceName = "sqTimeTable", allocationSize = 1)
@Table(indexes = {
        @Index(name = "idx_TimeTable_userId", columnList = "userId")
})
public class TimeTable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqTimeTable")
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @Column
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TimeTable.Status status;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "FK_TimeTable_User"), nullable = false)
    private User user;

    @OneToMany(mappedBy = "timeTable")
    private List<DayLine> dayLines = new ArrayList<>();

    public TimeTable() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TimeTable.Status getStatus() {
        return status;
    }

    public void setStatus(TimeTable.Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<DayLine> getDayLines() {
        return dayLines;
    }

    public void addDayLine(DayLine dayLine) {
        this.dayLines.add(dayLine);
        dayLine.setTimeTable(this);
    }

    public enum Status {
        DRAFT,
        DONE,
        ACTIVE,
        ARCHIVE
    }
}
