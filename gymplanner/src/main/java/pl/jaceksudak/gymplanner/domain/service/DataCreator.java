package pl.jaceksudak.gymplanner.domain.service;

import org.springframework.stereotype.Component;
import pl.jaceksudak.gymplanner.domain.*;

@Component
public class DataCreator {

    private final ActivityRepository activityRepository;
    private final DayLineRepository dayLineRepository;
    private final TimeTableRepository timeTableRepository;
    private final ActivityOccurrenceRepository activityOccurrenceRepository;

    public DataCreator(ActivityRepository activityRepository, DayLineRepository dayLineRepository, TimeTableRepository timeTableRepository, ActivityOccurrenceRepository activityOccurrenceRepository) {
        this.activityRepository = activityRepository;
        this.dayLineRepository = dayLineRepository;
        this.timeTableRepository = timeTableRepository;
        this.activityOccurrenceRepository = activityOccurrenceRepository;
    }

    public void createActivityOccurrence(DayLine dayLine, Activity activity, Integer index) {
        ActivityOccurrence activityOccurrence = new ActivityOccurrence();
        activityOccurrence.setDayLine(dayLine);
        activityOccurrence.setActivity(activity);
        activityOccurrence.setIndex(index);
        activityOccurrenceRepository.save(activityOccurrence);
    }

    public ActivityOccurrence createActivityOccurrence(Long dayLineId, Long activityId, Integer index) {
        ActivityOccurrence activityOccurrence = new ActivityOccurrence();
        activityOccurrence.setDayLine(dayLineRepository.getOne(dayLineId));
        activityOccurrence.setActivity(activityRepository.getOne(activityId));
        activityOccurrence.setIndex(index);
        return activityOccurrenceRepository.save(activityOccurrence);
    }

    public DayLine createDayLine(Integer index, TimeTable timeTable) {
        DayLine dayLine = new DayLine();
        dayLine.setIndex(index);
        timeTable.addDayLine(dayLine);
        return dayLineRepository.save(dayLine);
    }

    public Activity createActivity(User user, String instructorName, String activityName) {
        Activity activity = new Activity();
        activity.setInstructorName(instructorName);
        activity.setName(activityName);
        user.addActivity(activity);
        return activityRepository.save(activity);
    }

    public TimeTable createDraftTimetableWithDayLines(String name, User user) {
        TimeTable timeTable = new TimeTable();
        timeTable.setName(name);
        timeTable.setStatus(TimeTable.Status.DRAFT);
        user.addTimeTable(timeTable);
        timeTable = timeTableRepository.save(timeTable);
        createDayLine(0, timeTable);
        createDayLine(1, timeTable);
        createDayLine(2, timeTable);
        createDayLine(3, timeTable);
        createDayLine(4, timeTable);

        return timeTable;
    }
}
