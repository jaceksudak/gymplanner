package pl.jaceksudak.gymplanner.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "sqDayLine", sequenceName = "sqDayLine", allocationSize = 1)
@Table(indexes = {
        @Index(name = "idx_DayLine_timeTableId", columnList = "timeTableId")
})
public class DayLine {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqDayLine")
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @Column(name = "`index`", nullable = false)
    private Integer index;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "timeTableId", foreignKey = @ForeignKey(name = "FK_DayLine_TimeTable"), nullable = false)
    private TimeTable timeTable;

    @OneToMany(mappedBy = "dayLine")
    private List<ActivityOccurrence> activityOccurrences = new ArrayList<>();

    public DayLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public TimeTable getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(TimeTable timeTable) {
        this.timeTable = timeTable;
    }

    public List<ActivityOccurrence> getActivityOccurrences() {
        return activityOccurrences;
    }

    public void setActivityOccurrences(List<ActivityOccurrence> activityOccurrences) {
        this.activityOccurrences = activityOccurrences;
    }
}
