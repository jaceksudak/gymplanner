package pl.jaceksudak.gymplanner.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "sqActivity", sequenceName = "sqActivity", allocationSize = 1)
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqActivity")
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @Column(nullable = false)
    private String name;

    @Column
    private String instructorName;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "FK_Activity_User"), nullable = false)
    private User user;

    @OneToMany(mappedBy = "activity")
    private List<ActivityOccurrence> activityOccurrences = new ArrayList<>();

    public Activity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ActivityOccurrence> getActivityOccurrences() {
        return activityOccurrences;
    }

    public void setActivityOccurrences(List<ActivityOccurrence> activityOccurrences) {
        this.activityOccurrences = activityOccurrences;
    }
}
