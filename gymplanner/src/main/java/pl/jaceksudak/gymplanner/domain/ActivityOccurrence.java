package pl.jaceksudak.gymplanner.domain;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "sqActivityOccurrence", sequenceName = "sqActivityOccurrence", allocationSize = 1)
@Table(indexes = {
        @Index(name = "idx_ActivityOccurrence_dayLineId", columnList = "dayLineId"),
        @Index(name = "idx_ActivityOccurrence_activityId", columnList = "activityId")
})
public class ActivityOccurrence {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sqActivityOccurrence")
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @Column(name = "`index`", nullable = false)
    private Integer index;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "dayLineId", foreignKey = @ForeignKey(name = "FK_ActivityOccurrence_DayLine"), nullable = false)
    private DayLine dayLine;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "activityId", foreignKey = @ForeignKey(name = "FK_ActivityOccurrence_Activity"), nullable = false)
    private Activity activity;

    public ActivityOccurrence() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public DayLine getDayLine() {
        return dayLine;
    }

    public void setDayLine(DayLine dayLine) {
        this.dayLine = dayLine;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
