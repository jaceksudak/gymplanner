package pl.jaceksudak.gymplanner.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityOccurrenceRepository extends JpaRepository<ActivityOccurrence, Long> {

    @Query(nativeQuery = true, value = "SELECT ao.* FROM [ActivityOccurrence] ao WHERE ao.dayLineId = ?1 ORDER BY [index] ASC")
    List<ActivityOccurrence> findAllByDayLineId(Long dayLineId);


    @Query(nativeQuery = true, value =
            "SELECT CASE WHEN EXISTS (" +
                    "    SELECT *" +
                    "    FROM [ActivityOccurrence] ao" +
                    "    JOIN [Activity] a ON ao.activityId = a.id" +
                    "    WHERE a.userId = ?2 AND ao.id = ?1" +
                    ")" +
                    "THEN CAST(1 AS BIT)" +
                    "ELSE CAST(0 AS BIT) END")
    boolean activityOccurrenceBelongsToUser(Long activityOccurrenceId, Long userId);
}
