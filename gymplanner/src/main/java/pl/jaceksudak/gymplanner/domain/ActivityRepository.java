package pl.jaceksudak.gymplanner.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    @Query(nativeQuery = true, value = "SELECT a.* FROM [Activity] a WHERE a.userId = ?1")
    List<Activity> findAllByUserId(Long userId);

    @Query(nativeQuery = true, value =
            "SELECT CASE WHEN EXISTS (" +
                    "    SELECT *" +
                    "    FROM [Activity] a" +
                    "    WHERE a.userId = ?2 AND a.id = ?1" +
                    ")" +
                    "THEN CAST(1 AS BIT)" +
                    "ELSE CAST(0 AS BIT) END")
    boolean activityBelongToUser(Long activityId, Long userId);

    @Modifying
    @Query(nativeQuery = true, value = "UPDATE [Activity] " +
            " SET [name] = ?2, [instructorName] = ?3 " +
            " WHERE [id] = ?1")
    void updateActivity(Long id, String activityName, String instructorName);
}
