package pl.jaceksudak.gymplanner.test;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;

import java.util.Random;

@Validated
@RestController
@RequestMapping(SecurityConstants.API_PATH + TestController.PATH)
public class TestController {

    public static final String PATH = "/test";

    @GetMapping("/endpoint")
    public Integer endpoint() {
        Random r = new Random();
        return r.nextInt();
    }
}