package pl.jaceksudak.gymplanner.security.common;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import pl.jaceksudak.gymplanner.security.UserPrincipal;

import java.util.Optional;

public class SecurityUtils {

    public static Optional<UserPrincipal> getUserPrincipal() {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> {
                    if (authentication.getPrincipal() instanceof UserPrincipal) {
                        return (UserPrincipal) authentication.getPrincipal();
                    }
                    return null;
                });
    }
}
