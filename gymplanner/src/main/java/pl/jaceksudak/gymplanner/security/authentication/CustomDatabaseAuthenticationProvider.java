package pl.jaceksudak.gymplanner.security.authentication;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.jaceksudak.gymplanner.domain.User;
import pl.jaceksudak.gymplanner.domain.UserRepository;
import pl.jaceksudak.gymplanner.security.UserData;

import java.util.Collections;
import java.util.Optional;

@Service
public class CustomDatabaseAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public CustomDatabaseAuthenticationProvider(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Optional<User> user = userRepository.findOneByLogin(username);
        if (user.isPresent()) {
            String saltedPassword = password + user.get().getSalt();
            if (passwordEncoder.matches(saltedPassword, user.get().getPassword())) {
                return new UsernamePasswordAuthenticationToken(new UserData(username, user.get().getId()), password, Collections.emptyList());
            }
        }
        throw new BadCredentialsException("Authentication failed");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
