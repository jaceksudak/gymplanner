package pl.jaceksudak.gymplanner.security.authentication;

import org.springframework.stereotype.Component;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;

import java.util.Random;

@Component
public class SaltGenerator {

    private static final int LEFT_ASCII_LIMIT = 48;
    private static final int RIGHT_ASCII_LIMIT = 122;

    public String generateSalt() {
        Random random = new Random();
        return random.ints(LEFT_ASCII_LIMIT, RIGHT_ASCII_LIMIT + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(SecurityConstants.SALT_SIZE)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
