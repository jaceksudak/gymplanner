package pl.jaceksudak.gymplanner.security;

import javax.validation.constraints.NotEmpty;

public class UserData {

    @NotEmpty
    private String name;

    @NotEmpty
    private Long userId;

    public UserData(@NotEmpty String name, @NotEmpty Long userId) {
        this.name = name;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
