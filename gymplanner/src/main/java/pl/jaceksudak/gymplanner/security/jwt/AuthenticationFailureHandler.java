package pl.jaceksudak.gymplanner.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import pl.jaceksudak.gymplanner.web.exception.model.ErrorKey;
import pl.jaceksudak.gymplanner.web.exception.model.ErrorModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFailureHandler implements AuthenticationEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFailureHandler.class);
    private final ObjectMapper mapper;

    public AuthenticationFailureHandler(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        LOGGER.error("Responding with unathorized error. Message - {}", authException.getMessage());

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        if (authException instanceof InsufficientAuthenticationException) {
            mapper.writeValue(response.getWriter(), new ErrorModel(HttpStatus.UNAUTHORIZED, ErrorKey.UNATHORIZED, "Not authorized", "unathorized access"));
        } else {
            mapper.writeValue(response.getWriter(), new ErrorModel(HttpStatus.UNAUTHORIZED, ErrorKey.UNATHORIZED, "Authentication failed", "authentication failed"));
        }
    }
}
