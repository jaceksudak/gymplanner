package pl.jaceksudak.gymplanner.security;

import javax.validation.constraints.NotEmpty;

public class UserPrincipal {

    @NotEmpty
    private UserData userData;

    @NotEmpty
    private String sessionId;

    @NotEmpty
    private String requestId;

    public UserPrincipal(@NotEmpty UserData userData, @NotEmpty String sessionId, @NotEmpty String requestId) {
        this.userData = userData;
        this.sessionId = sessionId;
        this.requestId = requestId;
    }

    public String getName() {
        return userData.getName();
    }

    public Long getUserId() {
        return userData.getUserId();
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getRequestId() {
        return requestId;
    }
}
