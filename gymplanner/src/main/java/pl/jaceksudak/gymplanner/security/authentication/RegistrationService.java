package pl.jaceksudak.gymplanner.security.authentication;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.jaceksudak.gymplanner.domain.User;
import pl.jaceksudak.gymplanner.domain.UserRepository;

import java.util.Optional;

@Service
public class RegistrationService {


    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final SaltGenerator saltGenerator;

    public RegistrationService(UserRepository userRepository, PasswordEncoder passwordEncoder, SaltGenerator saltGenerator) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.saltGenerator = saltGenerator;
    }

    public Optional<User> register(String login, String password) {
        if (userRepository.findOneByLogin(login).isPresent()) {
            return Optional.empty();
        }
        String salt = saltGenerator.generateSalt();
        String encodedPassword = passwordEncoder.encode(password + salt);
        return Optional.of(userRepository.save(new User(login, encodedPassword, salt)));
    }
}
