package pl.jaceksudak.gymplanner.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import pl.jaceksudak.gymplanner.config.JwtConfig;
import pl.jaceksudak.gymplanner.security.UserData;
import pl.jaceksudak.gymplanner.security.UserPrincipal;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.function.Function;

@Component
public class TokenService {

    private final JwtConfig jwtConfig;

    public TokenService(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    public String generateToken(Authentication authentication, String sessionId) {
        UserData userData = (UserData) authentication.getPrincipal();
        return generateToken(userData.getName(), userData.getUserId(), sessionId);
    }

    public String renewToken(UserPrincipal userPrincipal) {
        return generateToken(userPrincipal.getName(), userPrincipal.getUserId(), userPrincipal.getSessionId());
    }

    private String generateToken(String userName, Long userId, String sessionId) {
        return Jwts.builder()
                .setSubject(userName)
                .setIssuedAt(new Date())
                .setExpiration(Date.from(LocalDateTime.now()
                        .plusMinutes(jwtConfig.getTokenExpirationTime())
                        .atZone(ZoneId.systemDefault())
                        .toInstant()))
                .claim(SecurityConstants.SESSION_ID, sessionId)
                .claim(SecurityConstants.USER_ID, userId)
                .signWith(SignatureAlgorithm.HS512, jwtConfig.getTokenSigningKey())
                .compact();
    }

    public Optional<TokenDTO> getValidTokenFromRequest(HttpServletRequest request) {
        return extractTokenUsingTokenMapper(request, token -> {
            try {
                return createTokenDTO(getBody(token), token);
            } catch (Exception e) {
                return null;
            }
        });
    }

    public Optional<TokenDTO> getValidOrExpiredTokenFromRequest(HttpServletRequest request) {
        return extractTokenUsingTokenMapper(request,
                token -> {
            try {
                return createTokenDTO(getBody(token), token);
            } catch (ExpiredJwtException e) {
                return createTokenDTO(e.getClaims(), token);
            } catch (Exception e) {
                return null;
            }
        });
    }

    private Optional<TokenDTO> extractTokenUsingTokenMapper(HttpServletRequest request, Function<String, TokenDTO> mapper) {
        return Optional.ofNullable(request.getHeader(SecurityConstants.AUTHORIZATION_HEADER))
                .filter(StringUtils::hasText)
                .filter(header -> header.startsWith(SecurityConstants.TOKEN_PREFIX))
                .map(header -> header.replace(SecurityConstants.TOKEN_PREFIX, ""))
                .filter(StringUtils::hasText)
                .map(mapper);
    }

    private TokenDTO createTokenDTO(Claims body, String token) {
        TokenDTO dto = new TokenDTO();
        dto.setUserName(body.getSubject());
        dto.setSessionId(body.get(SecurityConstants.SESSION_ID, String.class));
        dto.setUserId(body.get(SecurityConstants.USER_ID, Long.class));
        dto.setExpirationDate(body.getExpiration()
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime());
        dto.setToken(token);

        return dto;
    }

    private Claims getBody(String token) {
        return Jwts.parser().setSigningKey(jwtConfig.getTokenSigningKey()).parseClaimsJws(token).getBody();
    }

    public boolean shouldBeRenew(TokenDTO tokenDTO) {
        return tokenDTO.getExpirationDate().minusMinutes(jwtConfig.getTokenRenewTime()).isBefore(LocalDateTime.now());
    }
}
