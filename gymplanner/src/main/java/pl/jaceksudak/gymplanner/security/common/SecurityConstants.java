package pl.jaceksudak.gymplanner.security.common;

public class SecurityConstants {

    public static final String API_PATH = "/api";

    public static final String SESSION_ID = "session_id";
    public static final String USER_ID = "user_id";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String SHOULD_BE_RENEW_HEADER = "Token-Status";
    public static final String SHOULD_BE_RENEW_VALUE = "refresh";
    public static final String SHOULD_BE_RENEW_ALLOW_HEADER = "Access-Control-Expose-Headers";

    public static final int SALT_SIZE = 50;
}
