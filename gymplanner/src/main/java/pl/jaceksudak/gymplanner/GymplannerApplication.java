package pl.jaceksudak.gymplanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.jaceksudak.gymplanner.web.authentication.dto.LoginRequest;
import pl.jaceksudak.gymplanner.web.authentication.dto.TokenResponse;

@SpringBootApplication
@EnableJpaRepositories
public class GymplannerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(GymplannerApplication.class, args);
	}
}
