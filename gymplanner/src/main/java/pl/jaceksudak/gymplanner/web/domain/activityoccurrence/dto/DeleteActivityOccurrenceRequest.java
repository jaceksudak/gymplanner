package pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto;

import javax.validation.constraints.NotNull;

public class DeleteActivityOccurrenceRequest {

    @NotNull
    private Long activityOccurrenceId;

    public DeleteActivityOccurrenceRequest() {
    }

    public DeleteActivityOccurrenceRequest(@NotNull Long activityOccurrenceId) {
        this.activityOccurrenceId = activityOccurrenceId;
    }

    public Long getActivityOccurrenceId() {
        return activityOccurrenceId;
    }
}
