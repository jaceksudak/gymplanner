package pl.jaceksudak.gymplanner.web.exception.model;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ErrorModel {

    private int httpCode;
    private HttpStatus httpStatus;
    private LocalDateTime timestamp;
    private String requestId;
    private String key;
    private String message;
    private String details;
    private List<ValidationError> validationErrors = new ArrayList<>(0);

    public ErrorModel() {
    }

    public ErrorModel(HttpStatus httpStatus, String key, String message, String details, List<ValidationError> validationErrors) {
        this.httpCode = httpStatus.value();
        this.httpStatus = httpStatus;
        this.timestamp = LocalDateTime.now();
        this.key = key;
        this.message = message;
        this.details = details;
        this.validationErrors = validationErrors;
    }

    public ErrorModel(HttpStatus httpStatus, String key, String message, String details) {
        this.httpCode = httpStatus.value();
        this.httpStatus = httpStatus;
        this.timestamp = LocalDateTime.now();
        this.key = key;
        this.message = message;
        this.details = details;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }
}
