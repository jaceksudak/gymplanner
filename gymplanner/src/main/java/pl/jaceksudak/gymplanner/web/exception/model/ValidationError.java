package pl.jaceksudak.gymplanner.web.exception.model;

public class ValidationError {

    private String path;
    private String key;
    private String message;
    private Object rejectedValue;

    public ValidationError() {
    }

    public ValidationError(String path, String key, String message, Object rejectedValue) {
        this.path = path;
        this.key = key;
        this.message = message;
        this.rejectedValue = rejectedValue;
    }

    public String getPath() {
        return path;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

    public Object getRejectedValue() {
        return rejectedValue;
    }
}
