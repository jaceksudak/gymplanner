package pl.jaceksudak.gymplanner.web.domain.activity.dto;


import javax.validation.constraints.NotEmpty;

public class ActivityDto {

    @NotEmpty
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String instructorName;

    public ActivityDto() {
    }

    public ActivityDto(@NotEmpty Long id, @NotEmpty String name, @NotEmpty String instructorName) {
        this.id = id;
        this.name = name;
        this.instructorName = instructorName;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getInstructorName() {
        return instructorName;
    }
}
