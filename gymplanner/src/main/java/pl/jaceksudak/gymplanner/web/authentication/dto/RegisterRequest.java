package pl.jaceksudak.gymplanner.web.authentication.dto;

import javax.validation.constraints.NotEmpty;

public class RegisterRequest {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

    public RegisterRequest() {
    }

    public RegisterRequest(@NotEmpty String login, @NotEmpty String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
