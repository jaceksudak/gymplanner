package pl.jaceksudak.gymplanner.web.domain.activityoccurrence;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.jaceksudak.gymplanner.domain.*;
import pl.jaceksudak.gymplanner.domain.service.DataCreator;
import pl.jaceksudak.gymplanner.security.UserPrincipal;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;
import pl.jaceksudak.gymplanner.security.common.SecurityUtils;
import pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto.ActivityOccurrenceDto;
import pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto.AddActivityOccurrenceRequest;
import pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto.DeleteActivityOccurrenceRequest;
import pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto.DayLinesDto;
import pl.jaceksudak.gymplanner.web.exception.InvalidRequestException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping(SecurityConstants.API_PATH + ActivityOccurrenceController.PATH)
public class ActivityOccurrenceController {

    public static final String PATH = "/activityOccurrence";

    private final DayLineRepository dayLineRepository;
    private final ActivityOccurrenceRepository activityOccurrenceRepository;
    private final ActivityRepository activityRepository;
    private final TimeTableRepository timeTableRepository;
    private final DataCreator dataCreator;

    public ActivityOccurrenceController(DayLineRepository dayLineRepository, ActivityOccurrenceRepository activityOccurrenceRepository, ActivityRepository activityRepository, TimeTableRepository timeTableRepository, DataCreator dataCreator) {
        this.dayLineRepository = dayLineRepository;
        this.activityOccurrenceRepository = activityOccurrenceRepository;
        this.activityRepository = activityRepository;
        this.timeTableRepository = timeTableRepository;
        this.dataCreator = dataCreator;
    }


    @GetMapping
    @Transactional
    public DayLinesDto getDayLinesWithActivityOccurrencesForTimeTable(@RequestParam("timeTableId") Long timeTableId) {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();

                    if (timeTableRepository.timeTableBelongsToUser(timeTableId, userId)) {
                        List<Long> dayLinesIds = dayLineRepository.findAllByTimeTableId(timeTableId)
                                .stream()
                                .map(DayLine::getId)
                                .collect(Collectors.toList());
                        DayLinesDto dayLinesDto = new DayLinesDto();
                        dayLinesDto.setTimeTableId(timeTableId);

                        for (Long dayLineId : dayLinesIds) {
                            List<ActivityOccurrenceDto> activityOccurrenceDtos = new ArrayList<>(10);
                            for (int i = 0; i < 10; i ++) {
                                activityOccurrenceDtos.add(new ActivityOccurrenceDto(null, i, dayLineId, null, null));
                            }
                            List<ActivityOccurrence> aoFromDayLine = activityOccurrenceRepository.findAllByDayLineId(dayLineId);
                            for (ActivityOccurrence ao : aoFromDayLine) {
                                activityOccurrenceDtos.set(ao.getIndex(), new ActivityOccurrenceDto(ao.getId(), ao.getIndex(), dayLineId, ao.getActivity().getName(), ao.getActivity().getInstructorName()));
                            }
                            dayLinesDto.addActivityOccurrencesPerDayLineList(activityOccurrenceDtos);
                        }

                        return dayLinesDto;
                    } else {
                        throw new InvalidRequestException("Unsuccessful attempt");
                    }

                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }


    @PutMapping
    @Transactional
    public ActivityOccurrenceDto addActivityOccurrenceForDayLineAndActivity(@RequestBody @Valid AddActivityOccurrenceRequest request) {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();
                    Long dayLineId = request.getDayLineId();
                    Long activityId = request.getActivityId();
                    Integer index = request.getIndex();

                    if (dayLineRepository.dayLineBelongsToUser(dayLineId, userId)
                            && activityRepository.activityBelongToUser(activityId, userId)) {
                        ActivityOccurrence ao = dataCreator.createActivityOccurrence(dayLineId, activityId, index);

                        return new ActivityOccurrenceDto(ao.getId(), ao.getIndex(), ao.getDayLine().getId(), ao.getActivity().getName(), ao.getActivity().getInstructorName());
                    } else {
                        throw new InvalidRequestException("Unsuccessful attempt");
                    }

                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }

    @DeleteMapping
    @Transactional
    public void deleteActivityOccurrenceForDayLineAndActivity(@RequestBody @Valid DeleteActivityOccurrenceRequest request) {
        Optional<UserPrincipal> userPrincipalOptional = SecurityUtils.getUserPrincipal();
        if (userPrincipalOptional.isPresent()) {
            UserPrincipal userPrincipal = userPrincipalOptional.get();
            Long userId = userPrincipal.getUserId();
            Long activityOccurrenceId = request.getActivityOccurrenceId();

            if (activityOccurrenceRepository.activityOccurrenceBelongsToUser(activityOccurrenceId, userId)) {
                activityOccurrenceRepository.deleteById(activityOccurrenceId);
            } else {
                throw new InvalidRequestException("Unsuccessful attempt");
            }
        } else {
            throw new InvalidRequestException("Unsuccessful attempt");
        }
    }
}
