package pl.jaceksudak.gymplanner.web.authentication.dto;

import javax.validation.constraints.NotEmpty;

public class LoginRequest {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

    public LoginRequest() {
    }

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
