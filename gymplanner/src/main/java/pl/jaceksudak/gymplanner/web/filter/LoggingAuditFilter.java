package pl.jaceksudak.gymplanner.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Order(1)
@Component
public class LoggingAuditFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAuditFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String uri = extractRequestUri(request);
        String method = extractRequestMethod(request);

        long start = System.currentTimeMillis();
        LOGGER.info("Start: request uri {} method {}", uri, method);
        try {
            chain.doFilter(request, response);
            LOGGER.info("End: request uri {} method {} [duration: {}ms]", request, method, getDuration(start));
        } catch (Exception e) {
            LOGGER.info("End: request uri {} method {} [duration: {}ms] with exception {}", request, method, getDuration(start), e.toString());
            throw e;
        }
    }

    private String extractRequestUri(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            return "UNKNOWN";
        } else {
            return ((HttpServletRequest) request).getRequestURI();
        }
    }

    private String extractRequestMethod(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            return "UNKNOWN";
        } else {
            return ((HttpServletRequest) request).getMethod();
        }
    }

    private long getDuration(long start) {
        return System.currentTimeMillis() - start;
    }


    @Override
    public void destroy() {
    }
}
