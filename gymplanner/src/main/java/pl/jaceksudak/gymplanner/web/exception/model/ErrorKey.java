package pl.jaceksudak.gymplanner.web.exception.model;

public class ErrorKey {

    public static final String ACCOUNT_BAD_CREDENTIALS = "gymplanner.account.error.bad_credentials";
    public static final String UNATHORIZED = "gymplanner.general.error.unathorized";

    public static final String FORBIDDEN = "gymplanner.general.error.forbidden";
    public static final String UNHANDLED_ERROR = "gymplanner.general.error.unhandled";
    public static final String VALIDATION_ERROR = "gymplanner.general.error.validation";
    public static final String INVALID_REQUEST = "gymplanner.general.error.invalid_request";
}
