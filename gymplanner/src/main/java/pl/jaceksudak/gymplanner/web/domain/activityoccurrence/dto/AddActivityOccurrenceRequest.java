package pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto;

import javax.validation.constraints.NotNull;

public class AddActivityOccurrenceRequest {

    @NotNull
    private Long activityId;

    @NotNull
    private Long dayLineId;

    @NotNull
    private Integer index;

    public AddActivityOccurrenceRequest() {
    }

    public AddActivityOccurrenceRequest(@NotNull Long activityId, @NotNull Long dayLineId, @NotNull Integer index) {
        this.activityId = activityId;
        this.dayLineId = dayLineId;
        this.index = index;
    }

    public Long getActivityId() {
        return activityId;
    }

    public Long getDayLineId() {
        return dayLineId;
    }

    public Integer getIndex() {
        return index;
    }
}
