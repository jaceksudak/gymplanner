package pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto;

import javax.validation.constraints.NotEmpty;

public class ActivityOccurrenceDto {

    private Long id;

    @NotEmpty
    private Integer index;

    @NotEmpty
    private Long dayLineId;

    private String activityName;

    private String activityInstructorName;

    public ActivityOccurrenceDto() {
    }

    public ActivityOccurrenceDto(Long id, @NotEmpty Integer index, @NotEmpty Long dayLineId, String activityName, String activityInstructorName) {
        this.id = id;
        this.index = index;
        this.dayLineId = dayLineId;
        this.activityName = activityName;
        this.activityInstructorName = activityInstructorName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Long getDayLineId() {
        return dayLineId;
    }

    public void setDayLineId(Long dayLineId) {
        this.dayLineId = dayLineId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityInstructorName() {
        return activityInstructorName;
    }

    public void setActivityInstructorName(String activityInstructorName) {
        this.activityInstructorName = activityInstructorName;
    }
}
