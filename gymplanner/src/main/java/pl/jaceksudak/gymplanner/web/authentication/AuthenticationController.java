package pl.jaceksudak.gymplanner.web.authentication;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.jaceksudak.gymplanner.domain.User;
import pl.jaceksudak.gymplanner.example.ExampleDataCreator;
import pl.jaceksudak.gymplanner.mdc.MdcTools;
import pl.jaceksudak.gymplanner.security.authentication.RegistrationService;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;
import pl.jaceksudak.gymplanner.security.common.SecurityUtils;
import pl.jaceksudak.gymplanner.security.jwt.TokenService;
import pl.jaceksudak.gymplanner.web.authentication.dto.LoginRequest;
import pl.jaceksudak.gymplanner.web.authentication.dto.RegisterRequest;
import pl.jaceksudak.gymplanner.web.authentication.dto.TokenResponse;
import pl.jaceksudak.gymplanner.web.exception.InvalidRequestException;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Validated
@RestController
@RequestMapping(SecurityConstants.API_PATH)
public class AuthenticationController {

    public static final String LOGIN_PATH = "/login";
    public static final String REFRESH_TOKEN_PATH = "/refresh";
    public static final String REGISTER_PATH = "/register";

    private final AuthenticationManager authenticationManager;
    private final TokenService tokenService;
    private final RegistrationService registrationService;
    private final ExampleDataCreator exampleDataCreator;

    public AuthenticationController(AuthenticationManager authenticationManager, TokenService tokenService, RegistrationService registrationService, ExampleDataCreator exampleDataCreator) {
        this.authenticationManager = authenticationManager;
        this.tokenService = tokenService;
        this.registrationService = registrationService;
        this.exampleDataCreator = exampleDataCreator;
    }

    @PostMapping(LOGIN_PATH)
    public ResponseEntity<TokenResponse> authenticateUser(@RequestBody @Valid LoginRequest loginRequest) {
        String userName = loginRequest.getLogin();
        MdcTools.setUserId(userName);

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                userName,
                loginRequest.getPassword()
        );

        return Optional.ofNullable(authenticationManager.authenticate(usernamePasswordAuthenticationToken))
                .map(authentication -> {
                    String sessionId = UUID.randomUUID().toString();
                    MdcTools.setSessionId(sessionId);
                    String token = tokenService.generateToken(authentication, sessionId);

                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.add(SecurityConstants.AUTHORIZATION_HEADER, SecurityConstants.TOKEN_PREFIX + token);

                    return new ResponseEntity<>(new TokenResponse(token, sessionId), httpHeaders, HttpStatus.OK);
                })
                .orElseThrow(() -> new BadCredentialsException("Unsuccessful login attempt " + userName));
    }

    @PostMapping(REFRESH_TOKEN_PATH)
    public ResponseEntity<TokenResponse> renewToken() {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    String token = tokenService.renewToken(userPrincipal);

                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.add(SecurityConstants.AUTHORIZATION_HEADER, SecurityConstants.TOKEN_PREFIX + token);

                    return new ResponseEntity<>(new TokenResponse(token, userPrincipal.getSessionId()), httpHeaders, HttpStatus.OK);
                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful refresh attempt"));
    }

    @PostMapping(REGISTER_PATH)
    public void registerUser(@RequestBody @Valid RegisterRequest registerRequest) {
        Optional<User> registeredUser = registrationService.register(registerRequest.getLogin(), registerRequest.getPassword());
        if (registeredUser.isPresent()) {
            exampleDataCreator.createExampleTimetable(registeredUser.get());
        } else {
            throw new InvalidRequestException("Unsuccessful register attempt");
        }
    }

}
