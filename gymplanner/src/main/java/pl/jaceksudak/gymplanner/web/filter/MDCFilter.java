package pl.jaceksudak.gymplanner.web.filter;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.jaceksudak.gymplanner.mdc.MdcTools;
import pl.jaceksudak.gymplanner.security.jwt.TokenDTO;
import pl.jaceksudak.gymplanner.security.jwt.TokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class MDCFilter extends OncePerRequestFilter {

    private final TokenService tokenService;

    public MDCFilter(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            Optional<TokenDTO> tokenDTO = tokenService.getValidOrExpiredTokenFromRequest(request);
            if (tokenDTO.isPresent()) {
                MdcTools.fillMdc(tokenDTO.get());
            } else {
                MdcTools.fillAsAnonymous();
            }

            filterChain.doFilter(request, response);
        } finally {
            MdcTools.cleanUpMdc();
        }
    }
}
