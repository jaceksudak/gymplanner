package pl.jaceksudak.gymplanner.web.domain.timetable.dto;

import pl.jaceksudak.gymplanner.domain.TimeTable;

import javax.validation.constraints.NotEmpty;

public class TimeTableDto {

    @NotEmpty
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private TimeTable.Status status;

    public TimeTableDto() {
    }

    public TimeTableDto(@NotEmpty Long id, @NotEmpty String name, @NotEmpty TimeTable.Status status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TimeTable.Status getStatus() {
        return status;
    }
}
