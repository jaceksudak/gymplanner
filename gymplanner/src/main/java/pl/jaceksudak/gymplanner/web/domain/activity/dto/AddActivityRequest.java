package pl.jaceksudak.gymplanner.web.domain.activity.dto;

import javax.validation.constraints.NotEmpty;

public class AddActivityRequest {

    @NotEmpty
    private String instructorName;

    @NotEmpty
    private String activityName;

    public AddActivityRequest() {
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}
