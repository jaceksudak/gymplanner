package pl.jaceksudak.gymplanner.web.domain.timetable;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.jaceksudak.gymplanner.domain.TimeTable;
import pl.jaceksudak.gymplanner.domain.TimeTableRepository;
import pl.jaceksudak.gymplanner.domain.User;
import pl.jaceksudak.gymplanner.domain.UserRepository;
import pl.jaceksudak.gymplanner.domain.service.DataCreator;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;
import pl.jaceksudak.gymplanner.security.common.SecurityUtils;
import pl.jaceksudak.gymplanner.web.domain.timetable.dto.AddTimeTableRequest;
import pl.jaceksudak.gymplanner.web.domain.timetable.dto.TimeTableDto;
import pl.jaceksudak.gymplanner.web.exception.InvalidRequestException;
import pl.jaceksudak.gymplanner.web.exception.model.ErrorKey;
import pl.jaceksudak.gymplanner.web.exception.model.ValidationError;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping(SecurityConstants.API_PATH + TimeTableController.PATH)
public class TimeTableController {

    public static final String PATH = "/timetable";

    private final DataCreator dataCreator;
    private final UserRepository userRepository;
    private final TimeTableRepository timeTableRepository;

    public TimeTableController(DataCreator dataCreator, UserRepository userRepository, TimeTableRepository timeTableRepository) {
        this.dataCreator = dataCreator;
        this.userRepository = userRepository;
        this.timeTableRepository = timeTableRepository;
    }

    @GetMapping
    public List<TimeTableDto> getAllTimeTables() {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();

                    return timeTableRepository.findAllByUserId(userId)
                            .stream()
                            .map(timeTable -> new TimeTableDto(timeTable.getId(), timeTable.getName(), timeTable.getStatus()))
                            .collect(Collectors.toList());
                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }

    @PutMapping
    @Transactional
    public TimeTableDto addTimeTable(@RequestBody @Valid AddTimeTableRequest request) {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();
                    User user = userRepository.getOne(userId);

                    if (timeTableRepository.timeTableWithNameExists(request.getTimeTableName())){
                        throw new InvalidRequestException("Unsuccessful attempt");
                    }

                    TimeTable timeTable = dataCreator.createDraftTimetableWithDayLines(request.getTimeTableName(), user);
                    return new TimeTableDto(timeTable.getId(), timeTable.getName(), timeTable.getStatus());
                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }

}
