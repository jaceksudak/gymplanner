package pl.jaceksudak.gymplanner.web.domain.activity;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.jaceksudak.gymplanner.domain.Activity;
import pl.jaceksudak.gymplanner.domain.ActivityRepository;
import pl.jaceksudak.gymplanner.domain.User;
import pl.jaceksudak.gymplanner.domain.UserRepository;
import pl.jaceksudak.gymplanner.domain.service.DataCreator;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;
import pl.jaceksudak.gymplanner.security.common.SecurityUtils;
import pl.jaceksudak.gymplanner.web.domain.activity.dto.AddActivityRequest;
import pl.jaceksudak.gymplanner.web.domain.activity.dto.ActivityDto;
import pl.jaceksudak.gymplanner.web.domain.activity.dto.EditActivityRequest;
import pl.jaceksudak.gymplanner.web.exception.InvalidRequestException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping(SecurityConstants.API_PATH + ActivityController.PATH)
public class ActivityController {

    public static final String PATH = "/activity";

    private final DataCreator dataCreator;
    private final UserRepository userRepository;
    private final ActivityRepository activityRepository;

    public ActivityController(DataCreator dataCreator, UserRepository userRepository, ActivityRepository activityRepository) {
        this.dataCreator = dataCreator;
        this.userRepository = userRepository;
        this.activityRepository = activityRepository;
    }

    @PutMapping
    @Transactional
    public ActivityDto addActivity(@RequestBody @Valid AddActivityRequest request) {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();
                    User user = userRepository.getOne(userId);

                    Activity activity = dataCreator.createActivity(user, request.getInstructorName(), request.getActivityName());

                    return new ActivityDto(activity.getId(), activity.getName(), activity.getInstructorName());
                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }

    @PatchMapping
    @Transactional
    public ActivityDto editActivity(@RequestBody @Valid EditActivityRequest request) {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();

                    if (activityRepository.activityBelongToUser(request.getId(), userId)) {
                        activityRepository.updateActivity(request.getId(), request.getActivityName(), request.getInstructorName());
                        return new ActivityDto(request.getId(), request.getActivityName(), request.getInstructorName());
                    } else {
                        throw new InvalidRequestException("Unsuccessful attempt");
                    }
                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }

    @GetMapping
    public List<ActivityDto> getAllActivities() {
        return SecurityUtils.getUserPrincipal()
                .map(userPrincipal -> {
                    Long userId = userPrincipal.getUserId();

                    return activityRepository.findAllByUserId(userId)
                            .stream()
                            .map(activity -> new ActivityDto(activity.getId(), activity.getName(), activity.getInstructorName()))
                            .collect(Collectors.toList());
                })
                .orElseThrow(() -> new InvalidRequestException("Unsuccessful attempt"));
    }
}
