package pl.jaceksudak.gymplanner.web.filter;

import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.jaceksudak.gymplanner.mdc.MdcTools;
import pl.jaceksudak.gymplanner.security.UserData;
import pl.jaceksudak.gymplanner.security.UserPrincipal;
import pl.jaceksudak.gymplanner.security.common.SecurityConstants;
import pl.jaceksudak.gymplanner.security.jwt.TokenDTO;
import pl.jaceksudak.gymplanner.security.jwt.TokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import static pl.jaceksudak.gymplanner.web.authentication.AuthenticationController.*;

@Component
@Order(0)
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private final TokenService tokenService;

    public TokenAuthenticationFilter(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String requestPath = request.getRequestURI();
        requestPath = requestPath.contains("?") ? requestPath.substring(0, requestPath.indexOf("?") + 1) : requestPath;

        final boolean isRegisterOrLoginRequest = requestPath.endsWith(SecurityConstants.API_PATH + LOGIN_PATH)
                || requestPath.endsWith(SecurityConstants.API_PATH + REGISTER_PATH) ;
        final boolean isRefreshTokenRequest = requestPath.endsWith(SecurityConstants.API_PATH + REFRESH_TOKEN_PATH);

        if (!isRegisterOrLoginRequest) {
            Optional<TokenDTO> optionalTokenDTO = tokenService.getValidTokenFromRequest(request);
            optionalTokenDTO.ifPresent(tokenDTO -> {
                authenticateUser(request, tokenDTO);
                if (!isRefreshTokenRequest && tokenService.shouldBeRenew(tokenDTO)) {
                    response.addHeader(SecurityConstants.SHOULD_BE_RENEW_HEADER, SecurityConstants.SHOULD_BE_RENEW_VALUE);
                    response.addHeader(SecurityConstants.SHOULD_BE_RENEW_ALLOW_HEADER, SecurityConstants.SHOULD_BE_RENEW_HEADER);
                }
            });

        }

        filterChain.doFilter(request, response);
    }

    private void authenticateUser(HttpServletRequest request, TokenDTO tokenDTO) {
        UserPrincipal userPrincipal = new UserPrincipal(new UserData(tokenDTO.getUserName(), tokenDTO.getUserId()), tokenDTO.getSessionId(), MdcTools.getRequestId());

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userPrincipal, tokenDTO.getToken(), createAuthorities(tokenDTO));
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    private Collection<? extends GrantedAuthority> createAuthorities(TokenDTO tokenDTO) {
        return tokenDTO
                .getPermissions()
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
