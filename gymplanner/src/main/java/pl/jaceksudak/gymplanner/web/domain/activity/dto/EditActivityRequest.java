package pl.jaceksudak.gymplanner.web.domain.activity.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class EditActivityRequest {

    @NotNull
    private Long id;

    @NotEmpty
    private String instructorName;

    @NotEmpty
    private String activityName;

    public EditActivityRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}
