package pl.jaceksudak.gymplanner.web.authentication.dto;

import javax.validation.constraints.NotEmpty;

public class TokenResponse {

    @NotEmpty
    private String accessToken;

    @NotEmpty
    private String sessionId;

    public TokenResponse() {
    }

    public TokenResponse(String accessToken, String sessionId) {
        this.accessToken = accessToken;
        this.sessionId = sessionId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getSessionId() {
        return sessionId;
    }
}
