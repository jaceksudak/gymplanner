package pl.jaceksudak.gymplanner.web.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;
import pl.jaceksudak.gymplanner.web.exception.InvalidRequestException;
import pl.jaceksudak.gymplanner.web.exception.model.ErrorKey;
import pl.jaceksudak.gymplanner.web.exception.model.ErrorModel;
import pl.jaceksudak.gymplanner.web.exception.model.ValidationError;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorModel> handleBadCredentials(BadCredentialsException ex) {
        LOGGER.info("Bad credentials " + ex);
        ErrorModel error = new ErrorModel(HttpStatus.UNAUTHORIZED, ErrorKey.ACCOUNT_BAD_CREDENTIALS,
                "Bad credentials", "Bad user or password");

        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorModel> handleAccessDenied(AccessDeniedException ex) {
        LOGGER.info("An error occurred processing reuqest " + ex);
        ErrorModel error = new ErrorModel(HttpStatus.FORBIDDEN, ErrorKey.FORBIDDEN,
                "Access forbidden", "Insufficient privileges to access the requested resource");

        return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<ErrorModel> handleInvalidRequest(InvalidRequestException ex) {
        LOGGER.info("An error occurred processing reuqest " + ex);
        ErrorModel error = new ErrorModel(HttpStatus.BAD_REQUEST, ErrorKey.INVALID_REQUEST,
                "Bad request", ex.getMessage());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.info("An error occurred processing request argument not valid " + ex);

        List<ValidationError> validationErrors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> new ValidationError(error.getField(), ErrorKey.VALIDATION_ERROR + "." + error.getCode(),
                        error.getDefaultMessage(), error.getRejectedValue()))
                .collect(Collectors.toList());
        ErrorModel error = new ErrorModel(HttpStatus.BAD_REQUEST, ErrorKey.VALIDATION_ERROR, "Validation error", ex.getBindingResult().toString(), validationErrors);

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }
        LOGGER.error("An error occurred processing request", ex);
        ErrorModel error = new ErrorModel(status, ErrorKey.UNHANDLED_ERROR, "Error", ex.getMessage());

        return new ResponseEntity<>(error, headers, status);
    }
}
