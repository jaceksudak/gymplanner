package pl.jaceksudak.gymplanner.web.domain.timetable.dto;

import javax.validation.constraints.NotEmpty;

public class AddTimeTableRequest {

    @NotEmpty
    private String timeTableName;

    public AddTimeTableRequest() {
    }

    public AddTimeTableRequest(@NotEmpty String timeTableName) {
        this.timeTableName = timeTableName;
    }

    public String getTimeTableName() {
        return timeTableName;
    }
}
