package pl.jaceksudak.gymplanner.web.domain.activityoccurrence.dto;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class DayLinesDto {

    @NotEmpty
    private Long timeTableId;

    @NotEmpty
    private List<List<ActivityOccurrenceDto>> activityOccurrencesPerDayLine = new ArrayList<>();

    public DayLinesDto() {
    }

    public DayLinesDto(@NotEmpty Long timeTableId, @NotEmpty List<List<ActivityOccurrenceDto>> activityOccurrencesPerDayLine) {
        this.timeTableId = timeTableId;
        this.activityOccurrencesPerDayLine = activityOccurrencesPerDayLine;
    }

    public Long getTimeTableId() {
        return timeTableId;
    }

    public void setTimeTableId(Long timeTableId) {
        this.timeTableId = timeTableId;
    }

    public List<List<ActivityOccurrenceDto>> getActivityOccurrencesPerDayLine() {
        return activityOccurrencesPerDayLine;
    }

    public void setActivityOccurrencesPerDayLine(List<List<ActivityOccurrenceDto>> activityOccurrencesPerDayLine) {
        this.activityOccurrencesPerDayLine = activityOccurrencesPerDayLine;
    }

    public void addActivityOccurrencesPerDayLineList(List<ActivityOccurrenceDto> activityOccurrenceDtos) {
        activityOccurrencesPerDayLine.add(activityOccurrenceDtos);
    }
}
