package pl.jaceksudak.gymplanner.common;

public class Header {

    public static final String USER_ID = "userId";
    public static final String SESSION_ID = "sessionId";
    public static final String REQUEST_ID = "requestId";
}
